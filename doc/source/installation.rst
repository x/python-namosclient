============
Installation
============

At the command line::

    $ pip install python-namosclient

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv python-namosclient
    $ pip install python-namosclient
